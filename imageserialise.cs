using System;
using System.Web.Script.Serialization;

namespace imageserialise {

  /// <summary>
  ///   The main entry point for the application
  /// </summary>
  [STAThread]
  public static void Main(string[] args)
  {
    OpenFileDialog image = new OpenFileDialog();
    string imageString = ImageToString(image.path);
    Console.WriteLine(imageString);
  }

  public string ImageToString(string path) {

  if (path == null)

  throw new ArgumentNullException("path");

  Image im = Image.FromFile(path);

  MemoryStream ms = new MemoryStream();

  im.Save(ms, im.RawFormat);

  byte[] array = ms.ToArray();

  return Convert.ToBase64String(array);
}

  public Image StringToImage(string imageString) {

  if (imageString == null)

  throw new ArgumentNullException("imageString");

  byte[] array = Convert.FromBase64String(imageString);

  Image image = Image.FromStream(new MemoryStream(array));

  return image;
}
}
