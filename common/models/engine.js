'use strict';
const tf = require('@tensorflow/tfjs');
const Canvas = require('canvas');
const labels = require('../../mapping.json');


require('@tensorflow/tfjs-node-gpu');

//require('@tensorflow/tfjs-node');



function cropImage(img) {
    const width = img.shape[0];
    const height = img.shape[1];

    if (width == height) {
        return img;
    }
    // use the shorter side as the size to which we will crop
    const shorterSide = Math.min(img.shape[0], img.shape[1]);

    // calculate beginning and ending crop points
    const startingHeight = (height - shorterSide) / 2;
    const startingWidth = (width - shorterSide) / 2;
    const endingHeight = startingHeight + shorterSide;
    const endingWidth = startingWidth + shorterSide;

    // return image data cropped to those points
    return img.slice([startingWidth, startingHeight, 0], [endingWidth, endingHeight, 3]);
}

function resizeImage(image) {
    return tf.image.resizeBilinear(image, [28, 28]);
}

function batchImage(image) {
    // Expand our tensor to have an additional dimension, whose size is 1
    const batchedImage = image.expandDims(0);

    // Turn pixel data into a float between -1 and 1.
    return batchedImage.toFloat().div(tf.scalar(127)).sub(tf.scalar(1));
}


function loadAndProcessImage(image) {
    const croppedImage = cropImage(image);
    const resizedImage = resizeImage(croppedImage);
    return batchImage(resizedImage);
}

module.exports = function(Engine) {

    Engine.evaluatereq = async function (data) {
        const network = await tf.loadModel("http://localhost:8080/model.json");
        if (!Buffer.isBuffer(data)) {
            const err = new Error('Unsupported media type');
            err.statusCode = 415;
            throw err;
        }
        const img = new Canvas.Image();
        const canvas = Canvas.createCanvas(28, 28);
        const ctx = canvas.getContext("2d");
        var response;
        img.onload = function () {
            ctx.drawImage(img, 0, 0, 28, 28);
            var tensor = tf.fromPixels(canvas, 1);
            tensor = loadAndProcessImage(tensor);
            tensor = tensor.reshape([1, 28, 28, 1]);
            var prediction = network.predict(tensor);
            const labelPrediction = prediction.as1D().argMax().dataSync()[0];
            response = [String.fromCharCode(labels[labelPrediction]), true]
        };
        img.src = data;
        return response;


    };

    Engine.evaluateimg = async function (container, name) {
        const network = await tf.loadModel("http://storage:8080/model.json");
        // Accepts image as a header of an array and converts to a tensor and processes
        const path = ('./server/storage/' + container + '/' + name + '.jpg');

        const img = new Canvas.Image();
        const canvas = Canvas.createCanvas(28, 28);
        const ctx = canvas.getContext("2d");
        var response;
        img.onload = function () {
            ctx.drawImage(img, 0, 0, 28, 28);
            var tensor = tf.fromPixels(canvas, 1);
            tensor = loadAndProcessImage(tensor);
            tensor = tensor.reshape([1, 28, 28, 1]);
            var prediction = network.predict(tensor);
            const labelPrediction = prediction.as1D().argMax().dataSync()[0];
            response = [String.fromCharCode(labels[labelPrediction]), true]
        };
        img.src = path;
        return response;


    };

    Engine.remoteMethod(
        'evaluateimg', {
            http:
                {path: '/evaluateimg', verb: 'post'},
            accepts: [
                {arg: 'container', type: 'string'},
                {arg: 'name', type: 'string'}
            ],
            returns: [
                {arg: 'prediction', type: 'string'},
                {arg: 'success', type: 'boolean'}
            ]
        }
    );
    Engine.remoteMethod(
        'evaluatereq', {
            http:
                {path: '/evaluatereq', verb: 'post'},
            accepts: [
                {arg: 'data', type: 'object', http: {source: 'body'}},
            ],
            returns: [
                {arg: 'prediction', type: 'string'},
                {arg: 'success', type: 'boolean'}
            ]
        }
    )
};
